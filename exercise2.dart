import 'dart:io';
import 'dart:mirrors';

void lottery(String a) {
  if (a == '436594') {
    print('1st prize');
  } else if (a.substring(0, 3) == '893' || a.substring(0, 3) == '266') {
    print('The first three digits prize.');
  } else if (a.substring(3, 6) == '893' || a.substring(3, 6) == '266') {
    print('The last three digits prize.');
  } else if (a == '436595' || a == '436593') {
    print('The near 1st prize.');
  } else if (a == '049364' ||
      a == '396501' ||
      a == '285563' ||
      a == '084971' ||
      a == '502412') {
    print('The 2nd prize.');
  } else if (a == '049364' ||
      a == '996939' ||
      a == '166270' ||
      a == '043691' ||
      a == '896753' ||
      a == '575619') {
    print('The 3rd prize.');
  } else if (a == '049364' ||
      a == '450860' ||
      a == '567966' ||
      a == '991993' ||
      a == '240354' ||
      a == '403115') {
    print('The 4th prize.');
  } else if (a == '049364' ||
      a == '041233' ||
      a == '904680' ||
      a == '961523' ||
      a == '155735' ||
      a == '545548') {
    print('The 5th prize');
  } else {
    print('No prize');
  }
}

void main() {
  print('Please input number');
  while (true) {
    String? input = (stdin.readLineSync()!);
    if (input.length != 6) {
      print('Please try again');
    } else {
      lottery(input);
      break;
    }
  }
}
