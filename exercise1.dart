import 'dart:io';

void prime(int a) {
  int i = 0;
  int j = 0;
  int check = 0;
  j = a ~/ 2;
  if (a == 0 || a == 1) {
    print(a.toString() + " is not prime number");
  } else {
    for (i = 2; i <= j; i++) {
      if (a % i == 0) {
        print(a.toString() + " is not prime number");
        check = 1;
        break;
      }
    }
    if (check == 0) {
      print(a.toString() + " is prime number");
    }
  }
}

void main() {
  print('Please input number.');
  int? input = int.parse(stdin.readLineSync()!);
  prime(input);
}
